#!/usr/bin/env python

import importlib
from inspect import getmembers, isclass, signature
from typing import Any, Callable, Iterator, Tuple, Type

from click import ParamType

from clicked import group
from clicked.extras.register import extra_modules


def get_extra_params() -> Iterator[Tuple[ParamType, Type]]:
    for _, mod in extra_modules.items():
        extra = importlib.import_module(f".{mod}", "clicked.extras")
        for _, obj in getmembers(extra):
            if isclass(obj) and issubclass(obj, ParamType):
                sig = signature(obj.convert)
                retval = sig.return_annotation
                yield (obj, retval)


def make_param_function(param_type: ParamType, ann: Type) -> Callable:
    def param_fn(value: Any) -> None:
        print(value)

    param_fn.__name__ = param_type.name.lower()
    param_fn.__doc__ = f"Print input passed through {param_type.name}"
    return param_fn


extra_fns = [make_param_function(p, t) for p, t in get_extra_params()]

main = group(extra_fns)


if __name__ == "__main__":
    main()
