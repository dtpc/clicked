#!/usr/bin/env python

import pandas as pd

from clicked import command


def fn(df: pd.DataFrame) -> None:
    assert isinstance(df, pd.DataFrame)
    print(df)


if __name__ == "__main__":
    command(fn)()
