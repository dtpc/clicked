#!/usr/bin/env python

import enum
from pathlib import Path
from typing import List, Optional

import clicked


class Colour(enum.Enum):
    red = 1
    blue = 2
    green = 3


def one(a: int) -> None:
    print(a + 1)


def parent(path: Path) -> None:
    print(path.parent)


def colour(c: Colour) -> None:
    print(f"{c.name} = {c.value}")


def four_four(
    num: int,
    flag: bool = False,
    colour: Colour = Colour.blue,
    name: Optional[str] = None,
) -> None:
    """Prints out some inputs."""
    print(num, flag, name)


def proc(path: List[Path]):
    print(path)


main = clicked.group(
    [proc, one, parent, colour, four_four], "Example of a clicked group.", version="3.3"
)


main2 = clicked.command(four_four, name="ff")


if __name__ == "__main__":
    main()
