"""Data operations of rasters."""

from inspect import Parameter, signature
from typing import Any, Callable, List, Optional

import click

from .dispatch import dispatch
from .extras.register import try_extras_register
from .utils import is_ann_list, is_ann_optional


def print_result(result: Any) -> None:
    """Print and return exit code."""
    if result is not None:
        print(result)


_ret_callback = print_result


def group(
    fns: List[Callable],
    name: Optional[str] = None,
    desc: Optional[str] = None,
    version: Optional[str] = None,
    return_callback: Optional[Callable[[Any], None]] = _ret_callback,
) -> click.core.Group:
    """Make a click CLI group from a list of functions."""

    def main() -> None:
        pass

    if name:
        main.__name__ = name

    if desc:
        main.__doc__ = desc

    if version is not None:
        main = click.version_option(version=version)(main)

    grp: click.Group = click.group()(main)

    for fn in fns:
        cmd = command(fn, return_callback=return_callback)
        grp.add_command(cmd)

    return grp


def command(
    fn: Callable,
    name: Optional[str] = None,
    desc: Optional[str] = None,
    return_callback: Optional[Callable[[Any], None]] = _ret_callback,
) -> click.core.Command:
    """Make a click CLI command from a function."""
    cli = cli_fn(fn, desc=desc, return_callback=return_callback)
    if name is None:
        name = fn.__name__.replace("_", "-")
    cmd: click.Command = click.command(name=name)(cli)
    return cmd


def cli_fn(
    fn: Callable,
    desc: Optional[str] = None,
    return_callback: Optional[Callable[[Any], None]] = _ret_callback,
) -> Callable:
    """Wrap a function with click parameters."""

    def run_fn(**kwargs: Any) -> None:
        """Run function with keyword args and print the result."""
        result = fn(**kwargs)
        if return_callback is not None and result is not None:
            return_callback(result)

    run_fn.__name__ = f"run_{fn.__name__}"
    run_fn.__doc__ = desc or fn.__doc__

    sig = signature(fn)
    ps = [click_param_dec(p) for _, p in sig.parameters.items()]
    for p in reversed(ps):
        run_fn = p(run_fn)

    return run_fn


def click_param_dec(param: Parameter) -> Callable:
    """Make a click argument/option from a function parameter."""
    ann = param.annotation
    name = param.name.replace("_", "-")
    has_default = param.default != param.empty

    # optional
    is_opt = is_ann_optional(ann)
    if is_opt:
        if len(ann.__args__) > 2:
            ann.__args__ = ann.__args__[:-1]
        else:
            ann = ann.__args__[0]

    # list
    is_list = is_ann_list(ann)
    if is_list:
        ann = ann.__args__[0]

    required = not (is_opt or has_default)
    default = None if not has_default else param.default

    if ann == bool:
        if required:
            default = True
        name = f"--{name}/--no-{name}"
        a = click.option(name, default=default, show_default=True)
    else:
        if ann not in dispatch.registry:
            try_extras_register(ann)

        click_param = dispatch.dispatch(ann)(ann)
        if required:
            nargs = -1 if is_list else 1
            a = click.argument(name, type=click_param, required=required, nargs=nargs)
        else:

            a = click.option(
                f"--{name}",
                type=click_param,
                required=required,
                default=default,
                show_default=True,
                multiple=is_list,
            )
    return a
