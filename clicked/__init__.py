"""Automated CLI utility with click."""

from .core import command, group
from .dispatch import dispatch

__all__ = ("command", "group", "dispatch")
