"""A Click.ParamType for proj4 Coordinate Reference Systems (CRS)."""

from typing import Optional, Type

import click
from pyproj import Proj


class ClickCRS(click.ParamType):
    """A Click.ParamType for Coordinate Reference Systems.

    Converts a CLI parameter into a Proj object. The parameter can be either a
    proj4 string or an integer representing an EPSG code.
    """

    name = "CRS"

    def convert(
        self, value: str, param: Optional[click.Parameter], ctx: Optional[click.Context]
    ) -> Proj:
        """Convert value to Proj object if valid CRS."""
        try:
            try:
                p = Proj(init=f"epsg:{int(value)}")
                print(p)
            except ValueError:
                p = Proj(value)
        except RuntimeError:
            self.fail(f"{value} is not a valid CRS", param, ctx)

        return p


def register_fn(ann: Type) -> click.ParamType:
    """Param dispatch register function."""
    return ClickCRS()
