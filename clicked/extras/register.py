"""Dynamically register extras."""

import importlib
from typing import Any

from clicked.dispatch import dispatch
from clicked.utils import get_ann_name

extra_modules = {"Proj": "crs", "Dict": "dict", "DataFrame": "dataframe"}


def try_extras_register(ann: Any) -> bool:
    """Try to register the type from extras."""
    name = get_ann_name(ann)
    try:
        mod = extra_modules[name]
        extra = importlib.import_module(f".{mod}", "clicked.extras")
        dispatch.register(ann, getattr(extra, "register_fn"))
        success = True
    except KeyError:
        success = False
    return success
