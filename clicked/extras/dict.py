"""Convert yaml string or file to a dict."""

from pathlib import Path
from typing import Dict, Optional, Type

import click
import yaml


class ClickYamlDict(click.ParamType):
    """A click param for yaml strings or files."""

    name = "Yaml"

    def convert(
        self, value: str, param: Optional[click.Parameter], ctx: Optional[click.Context]
    ) -> Dict:
        """Convert yaml string or file to a dict."""
        value = value.strip()

        if Path(value).exists():
            with open(value, "r") as fh:
                d = yaml.load(fh)
        elif value.startswith("{") and value.endswith("}"):
            d = yaml.load(value)
        else:
            raise ValueError(f"Could not convert {value} to dict.")

        if not isinstance(d, dict):
            raise ValueError(f"{value} converts to type {type(d)}, not dict.")

        return d


def register_fn(ann: Type) -> click.ParamType:
    """Param dispatch register function."""
    return ClickYamlDict()
