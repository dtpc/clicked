"""Import CSV with pandas."""

from typing import Optional, Type

import click
import pandas as pd


class PandasCSV(click.File):
    """A click param for csv files to pandas df."""

    name = "CSV"

    def convert(
        self, value: str, param: Optional[click.Parameter], ctx: Optional[click.Context]
    ) -> pd.DataFrame:
        """Convert CSV file to pandas Dataframe."""
        fh = super().convert(value, param, ctx)
        df = pd.read_csv(fh)
        fh.close()
        return df


def register_fn(ann: Type) -> click.ParamType:
    """Param dispatch register function."""
    return PandasCSV()
