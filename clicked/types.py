"""Click parameter types."""

import enum
from pathlib import Path
from typing import Callable, List, Optional, Union

import click


class Unknown(click.ParamType):
    """A click param for unannotated types."""

    name = "Unknown"

    def convert(
        self, value: str, param: Optional[click.Parameter], ctx: Optional[click.Context]
    ) -> Union[int, float, str]:
        """Attempt to convert input to numeric type."""
        constructors = [int, float]
        for c in constructors:
            try:
                v: Union[int, str] = c(value)
                return v
            except (ValueError, TypeError):
                pass
        return value


class StrList(click.ParamType):
    """A click param for single char separated lists."""

    name = "StrList"

    def __init__(self, sep: str = ",") -> None:
        self.sep = sep

    def convert(
        self, value: str, param: Optional[click.Parameter], ctx: Optional[click.Context]
    ) -> List[str]:
        """Convert char separated string into list."""
        return None if value is None else value.split(self.sep)


class ClickEnum(click.Choice):
    """A click param converting strings to enums."""

    name = "Enum"

    def __init__(self, enum: enum.EnumMeta, str_op: Callable = str.format) -> None:
        super().__init__(enum.__members__)
        self.enum = enum
        self.str_op = str_op

    def convert(
        self, value: str, param: Optional[click.Parameter], ctx: Optional[click.Context]
    ) -> enum.Enum:
        """Convert str to enum."""
        value = super().convert(self.str_op(value), param, ctx)
        e: enum.Enum = self.enum[value]
        return e


class ClickPath(click.ParamType):
    """A click param for any path which returns a pathlib.Path."""

    name = "Path"
    click_param = click.Path()

    def convert(
        self, value: str, param: Optional[click.Parameter], ctx: Optional[click.Context]
    ) -> Path:
        """Convert path str to pathlib Path."""
        cvalue = self.click_param.convert(value, param, ctx)
        path = Path(cvalue).expanduser()
        return path


class ClickDir(ClickPath):
    """A click param for existing directory which returns a pathlib.Path."""

    name = "Dir"
    click_param = click.Path(exists=True, file_okay=False)


class ClickFile(ClickPath):
    """A click param for existing file which returns a pathlib.Path."""

    name = "File"
    click_param = click.Path(exists=True, dir_okay=False)
