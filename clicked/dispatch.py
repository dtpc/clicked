"""Dispatch types to click parameter types."""

import enum
from functools import singledispatch
from inspect import Parameter
from pathlib import Path
from typing import Type

import click

from .types import ClickEnum, ClickPath, Unknown


@singledispatch
def dispatch(ann: Type) -> click.ParamType:
    """Convert annotation to click parameter."""
    p = click.types.convert_type(ann)
    return p


dispatch.register(Parameter.empty, lambda _: Unknown())
dispatch.register(enum.Enum, lambda e: ClickEnum(e))
dispatch.register(Path, lambda _: ClickPath())
