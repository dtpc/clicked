"""Utils specific to python version."""

import sys
from typing import Any, List, Type, Union

_NoneType: type = type(None)

if sys.version_info >= (3, 7):

    def _is_generic_type(ann: Type, t: Any) -> bool:
        """Return True if `ann` is genertic type `t`."""
        return hasattr(ann, "__origin__") and bool(ann.__origin__ == t)

    def is_ann_optional(ann: Type) -> bool:
        """Return True if annotation is optional."""
        return _is_generic_type(ann, Union) and bool(ann.__args__[-1] == _NoneType)

    def is_ann_list(ann: Type) -> bool:
        """Return True if annotation is a List."""
        return _is_generic_type(ann, Union)

    def get_ann_name(ann: Type) -> str:
        """Get the name of the annotation."""
        if hasattr(ann, "_name"):
            return str(ann._name)
        return ann.__name__


else:
    from typing import GenericMeta

    def is_ann_optional(ann: Type) -> bool:
        """Return True if annotation is optional."""
        return type(ann) == type(Union) and ann.__args__[-1] == _NoneType

    def is_ann_list(ann: Type) -> bool:
        """Return True if annotation is a List."""
        return type(ann) == GenericMeta and ann.__base__ is List

    def get_ann_name(ann: Type) -> str:
        """Get the name of the annotation."""
        return ann.__name__
