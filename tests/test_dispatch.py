"""Test clicked dispatch of user types."""

from click.testing import CliRunner

from clicked import command

runner = CliRunner()


def test_dispatch_foo() -> None:
    class Foo:
        def __init__(self, bar: str) -> None:
            print(bar)
            self.bar = bar

    def foo_fn(foo: Foo) -> None:
        assert isinstance(foo, Foo)

    result = runner.invoke(command(foo_fn), ["abc"])
    assert result.exception is None
    assert result.exit_code == 0
