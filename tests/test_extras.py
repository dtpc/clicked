"""Test extra param types."""

from typing import Any, Dict, Type

import pandas as pd
import pytest
from click.testing import CliRunner
from pyproj import Proj

from clicked import command

extra_vals = [
    (Proj, "+init=epsg:32751"),
    (Proj, "+proj=utm +zone=51 +south +datum=WGS84 +units=m +no_defs"),
    (Proj, "32751"),
    (Dict, "{key: value, key2: [1, 2]}"),
    (Dict, "./tests/resources/dict.yaml"),
    (pd.DataFrame, "./tests/resources/sample.csv"),
]


@pytest.mark.parametrize("ann,cli_value", extra_vals)
def test_extra(ann: Type, cli_value: str) -> None:
    def fn(value: ann) -> None:  # mypy: ignore
        assert isinstance(value, ann)

    runner = CliRunner()
    result = runner.invoke(command(fn), [cli_value])
    assert result.exception is None
    assert result.exit_code == 0
