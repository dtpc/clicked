"""Test clicked commands."""

from enum import Enum
from pathlib import Path
from typing import Any, Callable, List, Optional, Tuple

import pytest
from click.testing import CliRunner

from clicked import command, group

runner = CliRunner()


def assert_ret(val: Any) -> Callable[[Any], None]:
    def _callback(ret: Any) -> None:
        assert ret == val

    return _callback


def nothing_fn() -> None:
    """Do nothing."""
    return


def test_nothing() -> None:
    f = command(nothing_fn)
    result = runner.invoke(f, ["--help"])
    assert "Usage: nothing-fn [OPTIONS]" in result.output
    assert "Do nothing." "" in result.output
    assert result.exit_code == 0


def path_fn(path: Path) -> Path:
    assert isinstance(path, Path)
    return path


class Color(Enum):
    orange = 1
    grey = 2


def enum_fn(colour: Color) -> Color:
    assert isinstance(colour, Color)
    return colour


def optional_fn(opt: Optional[int]) -> Optional[int]:
    assert isinstance(opt, int) or opt is None
    return opt


def default_fn(opt: int = 4) -> int:
    assert isinstance(opt, int)
    return opt


fn_args_ret: List[Tuple[Callable, List[str], Any]] = [
    (path_fn, ["/path/to/file.txt"], Path("/path/to/file.txt")),
    (enum_fn, ["orange"], Color.orange),
    (optional_fn, [], None),
    (optional_fn, ["--opt", "1"], 1),
    (default_fn, [], 4),
    (default_fn, ["--opt", "1"], 1),
]


AnyFn = Callable[[Any], Any]


@pytest.mark.parametrize("fn,args,ret", fn_args_ret)
def test_command(fn: AnyFn, args: List[str], ret: Any) -> None:
    result = runner.invoke(command(fn, return_callback=assert_ret(ret)), args)
    assert result.output == ""
    assert result.exception is None
    assert result.exit_code == 0


def bool_fn(flag: bool) -> bool:
    assert isinstance(flag, bool)
    return flag


def bool_fn_default(flag: bool = False) -> bool:
    assert isinstance(flag, bool)
    return flag


def bool_fn_opt(flag: Optional[bool]) -> Optional[bool]:
    assert isinstance(flag, bool) or flag == None
    return flag


def bool_fn_opt_default(flag: Optional[bool] = False) -> bool:
    assert isinstance(flag, bool)
    return flag


def bool_fn_opt_default_none(flag: Optional[bool] = None) -> Optional[bool]:
    assert isinstance(flag, bool) or flag == None
    return flag


bool_fns_defaults = [
    (bool_fn, True),
    (bool_fn_default, False),
    (bool_fn_opt, None),
    (bool_fn_opt_default, False),
    (bool_fn_opt_default_none, None),
]


@pytest.mark.parametrize("arg", [None, "--flag", "--no-flag"])
@pytest.mark.parametrize("fn,default", bool_fns_defaults)
def test_bools(fn: AnyFn, arg: Optional[str], default: Optional[bool]) -> None:
    values = {None: default, "--flag": True, "--no-flag": False}
    cb = assert_ret(values[arg])
    args = [] if arg is None else [arg]
    result = runner.invoke(command(fn, return_callback=cb), args)
    assert result.output == ""
    assert result.exception is None
    assert result.exit_code == 0


def many_fn(a: int, b: str, c: float, d: Optional[Color], e: bool = False) -> None:
    assert isinstance(a, int)
    assert isinstance(b, str)
    assert isinstance(c, float)
    assert isinstance(d, Color) or d is None
    assert isinstance(e, bool)


multi_args = [
    "1 buffalo 2.789".split(),
    "1 buffalo 2.789 --d=orange".split(),
    "1 buffalo 2.789 --d=orange --no-e".split(),
    "1 buffalo 2.789 --d=grey --e".split(),
]


@pytest.mark.parametrize("args", multi_args)
def test_command_multi_args(args: List[str]) -> None:
    result = runner.invoke(command(many_fn), args)
    assert result.output == ""
    assert result.exception is None
    assert result.exit_code == 0


fns_argnames_opts = [
    (path_fn, " PATH", []),
    (enum_fn, " [orange|grey]", []),
    (optional_fn, "", [("--opt INTEGER",)]),
    (default_fn, "", [("--opt INTEGER", "[default: 4]")]),
    (bool_fn, "", [("--flag / --no-flag", "[default: True]")]),
    (bool_fn_default, "", [("--flag / --no-flag", "[default: False]")]),
    (bool_fn_opt, "", [("--flag / --no-flag",)]),
    (bool_fn_opt_default, "", [("--flag / --no-flag", "[default: False]")]),
    (bool_fn_opt_default_none, "", [("--flag / --no-flag",)]),
]


@pytest.mark.parametrize("fn,arg_names,options", fns_argnames_opts)
def test_command_help(
    fn: AnyFn, arg_names: str, options: List[Tuple[str, str]]
) -> None:
    fname = fn.__name__.replace("_", "-")
    spaces = " " * max([len(o[0]) - 6 for o in options] + [0])
    option_str = "".join([f"  {'  '.join(o)}\n" for o in options])
    help_str = (
        f"Usage: {fname} [OPTIONS]{arg_names}\n\n"
        f"Options:\n{option_str}"
        f"  --help  {spaces}Show this message and exit.\n"
    )
    result = runner.invoke(command(fn), ["--help"])
    assert result.output == help_str
    assert result.exception is None
    assert result.exit_code == 0


@pytest.mark.parametrize("fn,args,ret", fn_args_ret)
def test_group(fn: AnyFn, args: List[str], ret: Any) -> None:
    fns = [f for f, _, _ in fn_args_ret]
    grp = group(fns, return_callback=assert_ret(ret))
    grp_args = [fn.__name__.replace("_", "-")] + args
    result = runner.invoke(grp, grp_args)
    assert result.output == ""
    assert result.exception is None
    assert result.exit_code == 0


def test_group_version() -> None:
    fns = [f for f, _, _ in fn_args_ret]
    grp = group(fns, name="test_group", desc="Test group.", version="1.2.3")
    result = runner.invoke(grp, ["--version"])
    assert result.output == "test-group, version 1.2.3\n"
    assert result.exception is None
    assert result.exit_code == 0


def unannotated_fn(a, b, c=True):  # mypy: ignore
    assert a is not None
    assert b is not None


args_unann = [["1", "2"], ["1", "2", "--c", "3"]]


@pytest.mark.parametrize("args", args_unann)
def test_unannotated(args: List[str]) -> None:
    result = runner.invoke(command(unannotated_fn), args)
    assert result.output == ""
    assert result.exception is None
    assert result.exit_code == 0


def add(a: int, b: int) -> int:
    return a + b


def test_print_return() -> None:
    result = runner.invoke(command(add), ["3", "2"])
    assert result.output == "5\n"
    assert result.exception is None
    assert result.exit_code == 0
