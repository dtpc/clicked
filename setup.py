#!/usr/bin/env python

import versioneer
from setuptools import find_packages, setup

readme = open("README.rst").read()

setup(
    name="clicked",
    version=versioneer.get_version(),
    description="Automated CLI using click",
    long_description=readme,
    author="Dave Cole",
    url="",
    # Dependencies
    install_requires=["click>=7.0"],
    extras_require={
        "dev": [
            "black==18.9b0",
            "flake8-bugbear==18.2.0",
            "flake8-builtins==1.4.1",
            "flake8-comprehensions>=1.4.1",
            "flake8-docstrings>=1.1.0",
            "flake8-isort>=2.5",
            "flake8-per-file-ignores>=0.6",
            "mypy>=0.570",
            "pandas>=0.20.0",
            "pyproj>=1.9",
            "pytest>=3.1.3",
            "pytest-cov>=2.5.1",
            "pytest-flake8>=0.8.1",
            "pyyaml>=3.12",
            "versioneer>=0.18",
        ]
    },
    # Contents
    packages=find_packages(exclude=["test*"]),
    package_data={},
    test_suite="tests",
    ext_modules=[],
    # Other commands
    cmdclass=versioneer.get_cmdclass(),
)
