=======
clicked
=======

.. image:: https://gitlab.com/dtpc/clicked/badges/develop/pipeline.svg
   :target: https://gitlab.com/dtpc/clicked/commits/develop
   :alt: pipeline status
.. image:: https://gitlab.com/dtpc/clicked/badges/develop/coverage.svg
   :target: https://gitlab.com/dtpc/clicked/commits/develop
   :alt: coverage
|

Automated CLI using click_.

.. _click: http://click.pocoo.org

Requires Python >= 3.6.


Minimal example of an executable command line script wrapping `myfunc`:

.. code-block:: python

   #! /usr/bin/env python
   import clicked
   from mymodule import myfunc

   if __name__ == "__main__":
       clicked.command(myfunc)()